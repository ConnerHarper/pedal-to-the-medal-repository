﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; 

public class Score : MonoBehaviour
{
    // Public variables
    public Text scoreDisplay; 

    // Private variables
    private int scoreValue = 0; 

    // Function to add to the player's score
    // NOT automatically called by Unity - we need to call it ourselves in our code
    public void AddScore(int toAdd)
    {
        // Update the numerical value of the score
        scoreValue = scoreValue + toAdd;

        // Update the display of the score based on the numerical
        scoreDisplay.text = scoreValue.ToString();
    }
}
